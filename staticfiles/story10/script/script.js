setTimeout(function() {
    $(".loading").addClass("hide");
    window.scrollTo(0,0);
}, 1000);

$(document).ready(function () {
    if (localStorage.getItem('theme') == null) localStorage.setItem('theme', 0)
    if (localStorage.getItem('theme') == 0) {
        $('.theme').addClass('no-transition');
        $('.theme').removeClass('theme-set');
        $('.theme')[0].offsetHeight;
        $('.theme').removeClass('no-transition');
    } else {
        $('.theme').addClass('no-transition');
        $('.theme').addClass('theme-set');
        $('.theme')[0].offsetHeight;
        $('.theme').removeClass('no-transition');
    }

    $(window).scroll(function () {
        var y = $(window).scrollTop();
        $("#top-shadow").css({
            'display': 'block',
            'opacity': y / 20
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(".scroll").on('click', function () {
        if (this.hash !== "") {
            event.preventDefault();

            var hash = this.hash;
            console.log(hash);
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 70
            }, 800);
        }
    });

    $('.navbar-collapse a').click(function() {
        $(".navbar-collapse").collapse('hide');
    });

    $('.navbar-toggler').click(function() {
        var y = $(window).scrollTop();
        if ($(".navbar-collapse").is(":visible")) {
            if (y == 0) {
                $("#top-shadow").css({
                    'display': 'block',
                    'opacity': 0
                });
            }
        } else {
            $("#top-shadow").css({
                'display': 'block',
                'opacity': 1
            });
        }
    });

    $('.theme-swap').click(function() {
        if (localStorage.getItem('theme') == 0) {
            localStorage.setItem('theme', 1);
            $('.theme').addClass('theme-set')
        } else {
            localStorage.setItem('theme', 0);
            $('.theme').removeClass('theme-set')
        }
        // $('.theme').toggleClass('theme-set');
    });

    $('.collapser').click(function() {
        var target = $(this).attr("data-target");
        $('.expandable').each(function() {
            if ($(this).is(target)) {
                $(this).toggleClass('uncollapsed');
            } else {
                $(this).removeClass('uncollapsed');
            }
        });
    });
});
