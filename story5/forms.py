from django import forms
from django.utils import timezone

CATEGORIES = [
    ('Busy', 'Busy'),
    ('Free', 'Free'),
    ('Tentative', 'Tentative'),
    ('Priority', 'Priority')
]

class AddSchedule(forms.Form):
    error_messages = {
        'required': 'Please type'
    }

    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Title',
        'maxlength': "30"
    }

    date_attrs = {
        'type': 'date',
        'class': 'form-control',
        'placeholder': 'Date'
    }

    time_attrs = {
        'type': 'time',
        'class': 'form-control',
        'placeholder': 'Time'
    }

    location_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Location',
        'maxlength': "30"
    }

    category_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Category',
        'maxlength': "30"
    }

    desc_attrs = {
        'type': 'text',
        'rows': 3,
        'class': 'form-control',
        'placeholder': 'Description',
        'maxlength': "100"
    }

    name = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=name_attrs))
    date = forms.DateField(label='', required=True, widget=forms.DateInput(attrs=date_attrs), initial=timezone.now)
    time = forms.TimeField(label='', required=True, widget=forms.TimeInput(attrs=time_attrs), initial=timezone.now)
    location = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=location_attrs))
    category = forms.CharField(label='', required=True, max_length=30, widget=forms.Select(attrs=category_attrs, choices=CATEGORIES))
    desc = forms.CharField(label='', required=True, max_length=100, widget=forms.Textarea(attrs=desc_attrs))
