from django.db import models
from django.utils import timezone

class PersonalSchedule(models.Model):
    name = models.CharField(max_length=30)
    date = models.DateField(default=timezone.now)
    time = models.TimeField(default=timezone.now)
    location = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    desc = models.CharField(max_length=100)
