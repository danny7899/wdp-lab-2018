from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books/', views.books, name='books'),
    path('logout/', views.logout_page, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
]
