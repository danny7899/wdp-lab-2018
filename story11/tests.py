from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.contrib.auth import get_user, login
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve
from . import views

class Story11Test(TestCase):
  def test_home_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)
    user = User.objects.create_user(username='username', password='pass')
    user.save()
    self.client.login(username='username', password='pass')
    response = self.client.get('/')
    self.assertRedirects(response, '/books/')

  def test_book_page_exists(self):
    response = self.client.get('/books/')
    self.assertRedirects(response, '/')
    user = User.objects.create_user(username='username', password='pass')
    user.save()
    self.client.login(username='username', password='pass')
    response = self.client.get('/books/')
    self.assertEqual(response.status_code,200)

  def test_using_index_func(self):
    found = resolve('/')
    self.assertEqual(found.func, views.index)
    
  def test_using_book_func(self):
    found = resolve('/books/')
    self.assertEqual(found.func, views.books)

  def test_using_index_template(self):
    response = self.client.get('/')
    self.assertTemplateUsed(response, 'story11/index.html')

  def test_logout(self):
    response = self.client.get('/logout/')
    self.assertRedirects(response, '/')
    user = User.objects.create_user(username='username', password='pass')
    user.save()
    self.client.login(username='username', password='pass')
    self.assertTrue(get_user(self.client).is_authenticated)
    response = self.client.get('/logout/')
    self.assertFalse(get_user(self.client).is_authenticated)
    self.assertRedirects(response, '/')
