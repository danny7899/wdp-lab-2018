from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import logout

def index(request):
  if not request.user.is_authenticated:
    return render(request, 'story11/index.html')
  return HttpResponseRedirect('/books/')

def books(request):
  if request.user.is_authenticated:
    return render(request, 'story11/books.html')
  return HttpResponseRedirect('/')

def logout_page(request):
  if request.user.is_authenticated:
    logout(request)
  return HttpResponseRedirect('/')
