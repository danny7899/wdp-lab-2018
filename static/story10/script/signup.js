$(document).ready(function () {
    $("#registerForm").submit(function() {
        event.preventDefault()
        var data = $(this).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value
            return obj
        }, {})

        validateName($("#registerForm #name_field"))
        validateEmail($("#registerForm #email_field"))
        validatePass($("#registerForm #password_field"))
        validateVerPass($("#registerForm #ver_password_field"))
        if (allValid()) {
            postData(data)
        }
        
        
    })

    $("#registerForm #name_field").focusout(function() {
        validateName($(this))
    })

    $("#registerForm #email_field").focusout(function() {
        validateEmail($(this))
    })

    $("#registerForm #password_field").focusout(function() {
        validatePass($(this))
    })

    $("#registerForm #ver_password_field").focusout(function() {
        validateVerPass($(this))
    })

    function postData(data) {
        $("#registerForm .submit-btn").attr("disabled", "disabled")
        $.ajax({
            url: "api/email_check/?email=" + data["email"],
            success: function(response) {
                if (response.message.exists == "false") {
                    $.ajax({
                        url: "api/add_registrant/",
                        type: "POST",
                        data: {
                            csrfmiddlewaretoken: data.csrfmiddlewaretoken,
                            name: data["name"],
                            email: data["email"],
                            password: data["password"],
                        },
                        beforeSend: function(xhr, settings) {
                            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                                xhr.setRequestHeader("X-CSRFToken", data.csrfmiddlewaretoken)
                            }
                        },
                        success: function(response) {
                            $("#registerForm .submit-btn").removeAttr("disabled")
                            if (response.success == "true") {
                                $("#registerForm :input").each(function(i, input) {
                                    if ($(input).attr("name") != "csrfmiddlewaretoken") $(input).val("")
                                })
                                $(".alert-area").empty().append('<div class="alert alert-success alert-dismissible fade show" role="alert">\
                                                                    Registration successful!\
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                                                                    <span aria-hidden="true">&times</span>\
                                                                    </button>\
                                                                </div>')
                            }
                        },
                        error: function(response) {
                            $("#registerForm .submit-btn").removeAttr("disabled")
                            $(".alert-area").empty().append('<div class="alert alert-danger alert-dismissible fade show" role="alert">\
                            An error has occurred! Try again later.\
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                            <span aria-hidden="true">&times</span>\
                            </button>\
                            </div>')
                        },
                    })
                } else {
                    $("#registerForm .submit-btn").removeAttr("disabled")
                }
            },
            error: function() {
                $("#registerForm .submit-btn").removeAttr("disabled")
            }
        })
    }

    function allValid() {
        var invalid = false
        $("#registerForm :input").each(function(i, input) {
            invalid |= $(input).hasClass("is-invalid")
        })
        return !invalid
    }

    function validateName(name_field) {
        if ($(name_field).val().trim() == "") {
            $(name_field).addClass("is-invalid")
            $("#name-error-text").text("Please enter your name!")
        } else {
            $(name_field).removeClass("is-invalid")
            $("#name-error-text").text("")
        }
    }

    function validateEmail(email_field) {
        if ($(email_field).val().trim() == "") {
            $(email_field).addClass("is-invalid")
            $("#email-error-text").text("Plese enter your email!")
        } else if (!isEmail($(email_field).val())) {
            $(email_field).addClass("is-invalid")
            $("#email-error-text").text("Plese enter a valid email!")
        } else {
            $(email_field).removeClass("is-invalid")
            checkAvailable($(email_field))
        }
    }

    function validatePass(pass_field) {
        if (($(pass_field).val().trim() == "")) {
            $(pass_field).addClass("is-invalid")
            $("#password-error-text").text("Please enter your password!")
        } else if ($(pass_field).val().length < $(pass_field).attr('minlength')) {
            $(pass_field).addClass("is-invalid")
            $("#password-error-text").text("Your password has to be 8 or more characters long!")
        } else {
            $(pass_field).removeClass("is-invalid")
            $("#password-error-text").text("")
        }
    }

    function validateVerPass(ver_pass_field) {
        if (($(ver_pass_field).val().trim() == "")) {
            $(ver_pass_field).addClass("is-invalid")
            $("#password-error-text").text("Please enter your password!")
        } else if ($(ver_pass_field).val() != $($(ver_pass_field).attr('target-content')).val()) {
            $(ver_pass_field).addClass("is-invalid")
            $("#password-error-text").text("Passwords do not match!")
        } else {
            $(ver_pass_field).removeClass("is-invalid")
            $("#password-error-text").text("")
        }
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
        return regex.test(email)
    }

    function checkAvailable(email_field) {
        $.ajax({
            url: "api/email_check/?email=" + $(email_field).val(),
            success: function(response) {
                if (response.message.exists == "false") {
                    $(email_field).removeClass("is-invalid")
                    $("#email-error-text").text("")
                } else {
                    $(email_field).addClass("is-invalid")
                    $("#email-error-text").text("This email is already registered!")
                }
            },
        })
    }

    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))
    }
})
