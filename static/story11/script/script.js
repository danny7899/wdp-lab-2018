$(document).ready(function () {
    if (sessionStorage.favouriteCount == null) {
        sessionStorage.favouriteCount = 0;
    }
    showCount(sessionStorage.favouriteCount);

    $('.popover-dismiss').popover({
        trigger: 'focus'
    })

    var currentSearch = new URL(location.href).searchParams.get("q");
    if (currentSearch != "" && currentSearch != null) {
        $('#search-field').val(currentSearch);
        getBooks(currentSearch);
    } else {
        $('.not-found').hide();
        $('.query-error').hide();
        $(".loader").hide();
    }

    $('#book-search').submit(function() {
        event.preventDefault();
        var data = $(this).serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        if (data.query.trim() == "") return;
        window.history.replaceState("", "", "?q=" + data.query);
        getBooks(data.query);
    });

    function getBooks(query) {
        $(".submit-btn").addClass("disabled");
        $(".hint").hide();
        $('.query-error').hide();
        $(".book-item").remove();
        $('.not-found').hide();
        $(".loader").show();
        // favouriteCount = 0;
        // $(".favourite-count").text(favouriteCount);
        $(".favourite-count-card .favourite-star").removeClass("favourited");
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + query,
            success: function(result) {
                $(".loader").hide();
                if (result.totalItems != 0) {
                    for (var i = 0; i < result.items.length; i++) {
                        var description = result.items[i].volumeInfo.description;
                        var bookLink = result.items[i].volumeInfo.infoLink;
                        var imageLink = result.items[i].volumeInfo.imageLinks;
                        var title = result.items[i].volumeInfo.title;
                        var author = result.items[i].volumeInfo.authors;
                        if (description == undefined) description = "No description provided.";
                        if (imageLink == undefined) imageLink = "http://identifyla.lsu.edu/peopleimages/noimage.jpg"; else imageLink = imageLink.thumbnail;
                        if (title == undefined) title = "No title provided.";
                        if (author == undefined) author = "No author provided.";
                        $('.books-list').append('<tr class="book-item" title="Click for description.">\
                                                    <td align="center">\
                                                    <a href=' + bookLink + ' target="_blank" class="popover-desc"title="Description"\
                                                    data-toggle="popover" data-placement="right" data-content="' + description + '">\
                                                    <img title="Click to view book" width="auto" height="112px" src="' + imageLink + '">\
                                                    </img></a></td>\
                                                    <td>' + title + '</td>\
                                                    <td>' + author + '</td>\
                                                    <td align="center"><i class="material-icons favourite-star"\
                                                     id="favourite-button">star</i></td>\
                                                </tr>');
                    }
                    $('[data-toggle="popover"]').popover();
                } else {
                    $('.not-found').show().empty().append("No books related to <b>" + query + "</b> are found!");
                }
                $(".submit-btn").removeClass("disabled");
            },
            error: function() {
                $(".loader").hide();
                $('.query-error').show();
                $(".submit-btn").removeClass("disabled");
            }
        });
    }
    
    // book row clicked
    $(document).on("click", ".book-item", function() {
        $(event.target).siblings().children().popover('show');
    });

    // user clicks away
    $('body').on('click', function () {
        if ($(event.target).data('toggle') !== 'popover') { 
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    $(document).on("click", "#favourite-button", function() {
        if ($(event.target).hasClass("favourited")) {
            $(event.target).removeClass("favourited");
            sessionStorage.favouriteCount--;
        } else {
            $(event.target).addClass("favourited");
            sessionStorage.favouriteCount++;
        }
        showCount(sessionStorage.favouriteCount);
    });
    
    $(window).scroll(function () {
        var y = $(window).scrollTop();
        $("#top-shadow").css({
            'display': 'block',
            'opacity': y / 20
        });
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(".scroll").on('click', function () {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 70
            }, 800);
        }
    });

    $('.navbar-collapse a').click(function() {
        $(".navbar-collapse").collapse('hide');
    });

    $('.navbar-toggler').click(function() {
        var y = $(window).scrollTop();
        if ($(".navbar-collapse").is(":visible")) {
            if (y == 0) {
                $("#top-shadow").css({
                    'display': 'block',
                    'opacity': 0
                });
            }
        } else {
            $("#top-shadow").css({
                'display': 'block',
                'opacity': 1
            });
        }
    });

    $('.collapser').click(function() {
        var target = $(this).attr("data-target");
        $('.expandable').each(function() {
            if ($(this).is(target)) {
                $(this).toggleClass('uncollapsed');
            } else {
                $(this).removeClass('uncollapsed');
            }
        });
    });

    $('.logout-btn').click(function() {
        sessionStorage.removeItem('favouriteCount')
    })

    function showCount(count) {
        if (count != 0) {
            $(".favourite-count-card .favourite-star").addClass("favourited");
        } else {
            $(".favourite-count-card .favourite-star").removeClass("favourited");
        }
        $(".favourite-count").text(count);
    }
});
