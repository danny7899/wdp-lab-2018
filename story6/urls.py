from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('add-status/', views.add_status, name='add-status'),
    path('remove-status/<int:id>', views.remove_status, name='remove-status'),
]
