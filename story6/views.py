from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status

def index(request):
    response = {
        'status_form': StatusForm,
        'status': Status.objects.all().order_by('-creation'),
        'view_name': 'index'
    }
    return render(request, 'story6/index.html', response)

def profile(request):
    response = {
        'view_name': 'profile'
    }
    return render(request, 'story6/profile.html', response)


def add_status(request):
    data = StatusForm(request.POST)
    if (request.method == "POST"):
        if (data.is_valid()):
            cleaned_data = data.cleaned_data
            status = Status(text=cleaned_data['text'])
            status.save()
    return HttpResponseRedirect('/')

def remove_status(request, id=None):
    if (id):
        status = Status.objects.get(id=id)
        status.delete()
    return HttpResponseRedirect('/')
