from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from .forms import StatusForm
from .models import Status
from . import views

class Story6Test(TestCase):
    def test_status_page_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_model_can_create_new_status(self):
        Status.objects.create(text="Hello")
        counting_all_available_activity = Status.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    def test_form_valid_data(self):
        form_data = {'text': 'Hello'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_add_status_request(self):
        response = Client().post('/add-status/', {'text': 'TestStatus'})
        response = Client().get('/')
        self.assertContains(response, 'TestStatus')

    def test_remove_status_request(self):
        response = Client().post('/add-status/', {'text': 'TestStatus'})
        added_amount = Status.objects.all().count()
        response = Client().get('/remove-status/1')
        removed_amount = Status.objects.all().count()
        self.assertEquals(added_amount, 1)

    def test_profile_page_exists(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_using_index_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'story6/profile.html')

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, views.profile)


class Story6FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--dns-prefetch-disable')
        self.options.add_argument('--no-sandbox')        
        self.options.add_argument('--headless')
        self.options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=self.options, executable_path='./chromedriver')
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(3)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_status_post(self):
        input_field = self.browser.find_element_by_id("input-field")
        input_field.send_keys("Foo Bar")
        input_field.submit()
        self.browser.get(self.live_server_url)
        assert self.browser.page_source.find("Foo Bar")

    def test_head_element(self):
        test_head_element = self.browser.find_element_by_class_name("row-head")
        assert test_head_element is not None

    def test_mid_element(self):
        test_mid_element = self.browser.find_element_by_class_name("row-mid")
        assert test_mid_element is not None

    def test_headline_font_weight(self):
        headline = self.browser.find_element_by_class_name("headline")
        font_weight = headline.value_of_css_property("font-weight")
        assert font_weight == "700"

    def test_card_border_radius(self):
        card = self.browser.find_element_by_class_name("card")
        border_radius = card.value_of_css_property("border-radius")
        assert border_radius == "32px"
    