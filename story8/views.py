from django.shortcuts import render

# Create your views here.
def index(request):
    response = {
        'view_name': 'index',
    }

    return render(request, 'story8/index.html', response)


def more(request):
    response = {
        'view_name': 'more',
    }

    return render(request, 'story8/more.html', response)