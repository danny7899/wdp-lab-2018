from django.test import TestCase
from django.urls import resolve
from . import views

class Story8Test(TestCase):
  def test_home_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)

  def test_more_page_exists(self):
    response = self.client.get('/more/')
    self.assertEqual(response.status_code,200)

  def test_using_index_func(self):
    found = resolve('/')
    self.assertEqual(found.func, views.index)

  def test_using_more_func(self):
    found = resolve('/more/')
    self.assertEqual(found.func, views.more)

  def test_using_index_template(self):
    response = self.client.get('/')
    self.assertTemplateUsed(response, 'story8/index.html')

  def test_using_more_template(self):
    response = self.client.get('/more/')
    self.assertTemplateUsed(response, 'story8/more.html')

