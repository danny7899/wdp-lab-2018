from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('more/', views.more, name='more'),
    path('register/', views.register, name='register'),
    path('register/api/email_check/', views.email_check, name="email_check"),
    path('register/api/add_registrant/', views.add_register, name="register_add"),
]
