from django.shortcuts import render
from django.http import JsonResponse
from .forms import RegisterForm
from .models import Registrants

import re

# Create your views here.
def index(request):
  response = {
      'view_name': 'index',
  }

  return render(request, 'story10/index.html', response)
  
def more(request):
  response = {
      'view_name': 'more',
  }

  return render(request, 'story10/more.html', response)

def register(request):
  response = {
      'view_name': 'register',
      'form': RegisterForm,
  }

  return render(request, 'story10/register.html', response)

def email_check(request):
    email = request.GET.get('email', None)
    if not Registrants.objects.filter(email__iexact=email).exists():
        return JsonResponse(data={'success' : 'true', 'message': {'exists': 'false'}}, status=202)
    return JsonResponse(data={'success' : 'true', 'message': {'exists': 'true'}}, status=202)

def add_register(request):
    if (request.method == 'POST'):
        form = RegisterForm(request.POST)
        if (form.is_valid()):
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            if email_valid(email):
                if not Registrants.objects.filter(email__iexact=email).exists():
                    registrant = Registrants(name=name, email=email, password=password)
                    registrant.save()
                    return JsonResponse(data={'success': 'true'}, status=202)
                return JsonResponse(data={'success': 'false', 'message': 'email already registered'}, status=202)
        return JsonResponse(data={'error': 'true', 'message': 'invalid parameters'}, status=408)
    return JsonResponse(data={'error': 'true', 'message': 'invalid request method'}, status=408)

def email_valid(email):
    return re.match("^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$", email) != None
